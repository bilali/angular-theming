import { Injectable, RendererFactory2, Renderer2, Inject } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class ThemesService {
  private _mainTheme$: BehaviorSubject<string> = new BehaviorSubject('default');

  private _renderer: Renderer2;
  private head: HTMLElement;
  private body: HTMLElement;
  private themeLinks: HTMLElement[] = [];

  theme$: Observable<string>;

  constructor(
    rendererFactory: RendererFactory2,
    @Inject(DOCUMENT) document: Document
  ) {
    this.head = document.head;
    this.body = document.body;
    this._renderer = rendererFactory.createRenderer(null, null);
    this.theme$ = this._mainTheme$.asObservable();

    this.theme$.subscribe(async (theme) => {
      await this.loadCss(theme + '.css');
      if (this.themeLinks.length == 2)
        this._renderer.removeChild(this.head, this.themeLinks.shift());
    });
  }

  setTheme(name: string) {
    this._mainTheme$.next(name);
  }

  private async loadCss(filename: string) {
    return new Promise((resolve) => {
      const linkEl: HTMLElement = this._renderer.createElement('link');
      this._renderer.setAttribute(linkEl, 'rel', 'stylesheet');
      this._renderer.setAttribute(linkEl, 'type', 'text/css');
      this._renderer.setAttribute(linkEl, 'href', filename);
      this._renderer.setProperty(linkEl, 'onload', resolve);
      this._renderer.appendChild(this.head, linkEl);
      this.themeLinks = [...this.themeLinks, linkEl];
    });
  }
}
